import pandas
import matplotlib.pyplot as plt


def draw_plot():
    df = pandas.read_json('results.json')
    df['rate'] = df['macs'] / df['time']
    df.set_index('workers', inplace=True)
    df.groupby('processes')['rate'].plot(legend=True)
    plt.show()


if __name__ == '__main__':
    draw_plot()