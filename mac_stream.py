from fastapi import FastAPI
from starlette.responses import StreamingResponse
from settings import ConfigSet
app = FastAPI()


async def produce_macs():
    for i in range(ConfigSet.macs_cnt):
        yield f'{11728141925580-i:012x}\n'.encode().upper()


@app.get("/")
async def root():
    return StreamingResponse(produce_macs())
