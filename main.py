import re
from logging import getLogger
from random import randrange
from fastapi import FastAPI, Response, status, Path
import asyncio

app = FastAPI()
logger = getLogger()

@app.get("/device/check/{mac}")
async def device_check(
        response: Response,
        mac: str = Path(title="MAC addr", min_length=12, max_length=12, regex=r'^[0-9A-Fa-f]{12}$'),
):
    #  adding random delay
    logger.info(f'Checking {mac}')
    await asyncio.sleep(randrange(40, 60)/10)
    return {"mac": mac.upper(), "status": 200}
