import asyncio
import json
import multiprocessing
import logging
import psutil
from concurrent.futures import ProcessPoolExecutor
from time import time, sleep
from aiohttp import ClientSession
from settings import ConfigSet

check_uri = f'http://{ConfigSet.mac_check_host}:{ConfigSet.mac_check_port}{ConfigSet.mac_check_path}'
list_uri = f'http://{ConfigSet.mac_stream_host}:{ConfigSet.mac_stream_port}{ConfigSet.mac_stream_path}'

FORMAT = '%(asctime)-15s %(levelname)-8s - %(threadName)s - %(funcName)s - %(message)s'
logging.basicConfig(level=ConfigSet.log_level, format=FORMAT)
logger = logging.getLogger()


async def get_macs(ml: multiprocessing.Queue, flags: dict):
    async with ClientSession() as s:
        async with s.get(list_uri) as r:
            async for line in r.content:
                ml.put_nowait(line.strip().decode())
    flags['list_loaded'] = True

    
async def get_status(t_num, w_num: int, s: ClientSession,
                     flags: dict, ml: multiprocessing.Queue, results: list):
    while (not ml.empty() and flags["list_loaded"]) \
            or not flags["list_loaded"]:
        try:
            mac = ml.get(block=False)
        except IndexError:
            logger.warning(f"Worker {t_num}:{w_num}. No MAC addresses loaded.")
            await asyncio.sleep(1)
            continue
        logger.info(f'Worker {t_num}:{w_num}. Requesting {mac}')
        async with s.get(f'{check_uri}/{mac}') as r:
            if r.status == 200:
                results.append(await r.json())
            else:
                logger.warning(f"Got invalid response:{r.status}")
                

async def run_workers(t_num: int, flags: dict, ml: multiprocessing.Queue, results: list):
    logger.info(f"starting workers on process {t_num}")
    await asyncio.sleep(2 + 1 * t_num)
    async with ClientSession() as s:
        coros = [get_status(t_num, i, s, flags, ml, results) for i in range(ConfigSet.workers_cnt)]
        await asyncio.gather(*coros)


class MacChecker(object):
    # adding manager to support interprocess data sharing
    manager = multiprocessing.Manager()
    mac_addresses: multiprocessing.Queue
    results: list
    flags: dict
    wrk_cnt = ConfigSet.workers_cnt

    def __init__(self):
        self.mac_addresses = self.manager.Queue()
        self.results = self.manager.list()
        self.flags = self.manager.dict()
        self.flags['list_loaded'] = False

    def macs_for_mp(self):
        try:
            asyncio.run(get_macs(self.mac_addresses, self.flags))
        except KeyboardInterrupt:
            logger.debug("Ending task")

    def workers_for_mp(self, t):
        try:
            asyncio.run(run_workers(t, self.flags, self.mac_addresses, self.results), debug=False)
            logger.debug(f"Process {t}. Length is {len(self.results)}")
        except KeyboardInterrupt:
            logger.debug("Ending task")


def main():
    ts = time()
    mc = MacChecker()
    fs = []
    with ProcessPoolExecutor(max_workers=ConfigSet.thread_cnt + 1) as executor:
        executor.submit(mc.macs_for_mp)
        for i in range(ConfigSet.thread_cnt):
            executor.submit(mc.workers_for_mp, i)
    t = time() - ts
    logger.debug(f"Results Length is {len(mc.results)}")
    logger.debug(f"Run {ConfigSet.thread_cnt} processes, {ConfigSet.workers_cnt} workers"
                 f" in {ConfigSet.macs_cnt} request in {t}.")
    return t


if __name__ == '__main__':
    res = []
    for wc in range(300, 1201, 100):
        for pc in range(5, 16):
            ConfigSet.workers_cnt = wc
            ConfigSet.thread_cnt = pc
            ConfigSet.macs_cnt = wc * pc * 2
            logger.warning(f"P:{pc}. W:{wc}")
            psutil.cpu_percent()
            t = main()
            cpu_usage = psutil.cpu_percent()
            res.append({
                'workers': wc,
                'processes': pc,
                'time': t,
                'macs': wc * pc * 2,
                'cpu': cpu_usage,
            })
    with open('results.json', 'w') as f:
        json.dump(res, f)
