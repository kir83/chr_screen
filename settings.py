import logging


class ConfigSet:
    macs_cnt = 25000
    mac_stream_host = '0.0.0.0'
    mac_stream_port = 8080
    mac_stream_path = '/'
    mac_check_host = '0.0.0.0'
    mac_check_port = 8000
    mac_check_path = '/device/check'
    workers_cnt = 2000
    thread_cnt = 10
    log_level = logging.WARNING
