import asyncio
import logging
from time import time
from collections import deque
from aiohttp import ClientSession
from settings import ConfigSet

FORMAT = '%(asctime)-15s %(levelname)-8s - %(threadName)s - %(funcName)s - %(message)s'
logging.basicConfig(level=ConfigSet.log_level, format=FORMAT)
logger = logging.getLogger()


class ListCompleted(Exception):
    pass


# create singleton object for MAC list and results
class MacChecker(object):
    check_uri = f'http://{ConfigSet.mac_check_host}:{ConfigSet.mac_check_port}{ConfigSet.mac_check_path}'
    list_uri = f'http://{ConfigSet.mac_stream_host}:{ConfigSet.mac_stream_port}{ConfigSet.mac_stream_path}'
    mac_addresses = deque()
    results = []
    list_loaded = False
    wrk_cnt = ConfigSet.workers_cnt

    def queue_finished(self):
        return self.mac_addresses.__len__() == 0

    async def get_macs(self):
        async with ClientSession() as s:
            async with s.get(self.list_uri) as r:
                # asynchronously read lines from http stream
                async for line in r.content:
                    self.mac_addresses.append(line.strip().decode())
        self.list_loaded = True

    async def get_status(self, w_num: int, s: ClientSession):
        # check if macs are loaded and list is not completed
        while (self.mac_addresses.__len__() > 0 and self.list_loaded) or not self.list_loaded:
            try:
                mac = self.mac_addresses.pop()
            except IndexError:
                logger.warning(f"Worker {w_num}. No MAC addresses loaded.")
                await asyncio.sleep(1)
                continue
            logger.info(f'Worker {w_num}. Requesting {mac}')
            async with s.get(f'{self.check_uri}/{mac}') as r:
                if r.status == 200:
                    self.results.append(await r.json())

    async def run_workers(self):
        logger.info("starting workers")
        async with ClientSession() as s:
            coros = [self.get_status(i, s) for i in range(self.wrk_cnt)]
            await asyncio.gather(*coros)

    async def run(self):
        await asyncio.gather(self.get_macs(), self.run_workers())


if __name__ == '__main__':
    ts = time()
    mc = MacChecker()
    asyncio.run(mc.run())
    t = time() - ts
    logger.info(f"Run {ConfigSet.macs_cnt} in {t}")
    logger.info(f"Expected {ConfigSet.macs_cnt} results. Got {len(mc.results)}")
